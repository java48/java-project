package Controllers;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.media.Media;
import javafx.scene.media.MediaView;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

import javax.swing.*;
import java.util.concurrent.ThreadLocalRandom;

import java.util.*;

public class Look implements Initializable {


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        Random rannd = new Random();
        int rand1 = rannd.nextInt(11 + 1);
        switchScene(rand1);

    }

    Answers odgovor = new Answers();




    public Button BtnAnswer1;
    public Button BtnAnswer2;
    public Button BtnAnswer3;
    public Button BtnAnswer4;
    public Button SceneChanger;
    public Button quit;

    @FXML
    private MediaView mv;
    @FXML
    private Media me;
    @FXML
    private Label Answer;
    public MediaPlayer mp;
    @FXML


    String austria = new File("src/Videos/Austria.mp4").getAbsolutePath();
    String bahrein = new File("src/Videos/Bahrein.mp4").getAbsolutePath();
    String monaco = new File("src/Videos/Monte Carlo.mp4").getAbsolutePath();
    String italy = new File("src/Videos/Italy.mp4").getAbsolutePath();
    String barcelona = new File("src/Videos/Barcelona.mp4").getAbsolutePath();
    String brazil = new File("src/Videos/Brazil.mp4").getAbsolutePath();
    String germany = new File("src/Videos/Hockenhiem.mp4").getAbsolutePath();
    String hungary = new File("src/Videos/Hungaroriing.mp4").getAbsolutePath();
    String britain = new File("src/Videos/Silverstone.mp4").getAbsolutePath();
    String singapore = new File("src/Videos/Singapore.mp4").getAbsolutePath();
    String spa = new File("src/Videos/Spa.mp4").getAbsolutePath();
    String usa = new File("src/Videos/Usa.mp4").getAbsolutePath();


    public void switchScene(int i) {



        switch (i) {

            case 1:

                BtnAnswer1.setText((odgovor.questAfrica.get(ThreadLocalRandom.current().nextInt(1, 11 + 1))));
                BtnAnswer2.setText(odgovor.questAmericas.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer3.setText(odgovor.questasia.get(6));
                BtnAnswer4.setText(odgovor.questEurope.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer3.setOnAction(this::trueanswer);


                me = new Media(new File(bahrein).toURI().toString());
                mp = new MediaPlayer(me);
                mv.setMediaPlayer(mp);
                mp.play();
                break;

            case 2:

                BtnAnswer1.setText((odgovor.questasia.get(ThreadLocalRandom.current().nextInt(1, 11 + 1))));
                BtnAnswer3.setText(odgovor.questAmericas.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer2.setText(odgovor.questEurope.get(7));
                BtnAnswer4.setText(odgovor.questEurope.get(ThreadLocalRandom.current().nextInt(7, 12 + 1)));
                BtnAnswer2.setOnAction(this::trueanswer);


                me = new Media(new File(austria).toURI().toString());
                mp = new MediaPlayer(me);
                mv.setMediaPlayer(mp);
                mp.play();
                break;


            case 3:

                BtnAnswer2.setText(odgovor.questAmericas.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer3.setText(odgovor.questasia.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer1.setText(odgovor.questAfrica.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer4.setText(odgovor.questEurope.get(0));
                BtnAnswer4.setOnAction(this::trueanswer);

                me = new Media(new File(barcelona).toURI().toString());
                mp = new MediaPlayer(me);
                mv.setMediaPlayer(mp);
                mp.play();
                break;

            case 4:
                BtnAnswer1.setText(odgovor.questEurope.get(1));
                BtnAnswer4.setText(odgovor.questasia.get(ThreadLocalRandom.current().nextInt(1, 11 + 1)));
                BtnAnswer2.setText(odgovor.questAfrica.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer3.setText(odgovor.questAmericas.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer1.setOnAction(this::trueanswer);

                me = new Media(new File(monaco).toURI().toString());
                mp = new MediaPlayer(me);
                mv.setMediaPlayer(mp);
                mp.play();
                break;

            case 5:
                BtnAnswer1.setText(odgovor.questAmericas.get(2));
                BtnAnswer3.setText(odgovor.questasia.get(ThreadLocalRandom.current().nextInt(1, 11 + 1)));
                BtnAnswer2.setText(odgovor.questAfrica.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer4.setText(odgovor.questEurope.get(ThreadLocalRandom.current().nextInt(1, 12 + 1)));
                BtnAnswer1.setOnAction(this::trueanswer);

                me = new Media(new File(brazil).toURI().toString());
                mp = new MediaPlayer(me);
                mv.setMediaPlayer(mp);
                mp.play();
                break;

            case 6:
                BtnAnswer3.setText(odgovor.questEurope.get(3));
                BtnAnswer4.setText(odgovor.questasia.get(ThreadLocalRandom.current().nextInt(1, 11 + 1)));
                BtnAnswer2.setText(odgovor.questAfrica.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer1.setText(odgovor.questAmericas.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer3.setOnAction(this::trueanswer);

                me = new Media(new File(spa).toURI().toString());
                mp = new MediaPlayer(me);
                mv.setMediaPlayer(mp);
                mp.play();
                break;

            case 7:

                BtnAnswer2.setText(odgovor.questEurope.get(6));
                BtnAnswer3.setText(odgovor.questasia.get(ThreadLocalRandom.current().nextInt(1, 11 + 1)));
                BtnAnswer4.setText(odgovor.questAfrica.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer1.setText(odgovor.questAmericas.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer2.setOnAction(this::trueanswer);

                me = new Media(new File(germany).toURI().toString());
                mp = new MediaPlayer(me);
                mv.setMediaPlayer(mp);
                mp.play();
                break;

            case 8:

                BtnAnswer3.setText(odgovor.questasia.get(7));
                BtnAnswer2.setText(odgovor.questEurope.get(ThreadLocalRandom.current().nextInt(1, 13 + 1)));
                BtnAnswer4.setText(odgovor.questAfrica.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer1.setText(odgovor.questAmericas.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer3.setOnAction(this::trueanswer);

                me = new Media(new File(singapore).toURI().toString());
                mp = new MediaPlayer(me);
                mv.setMediaPlayer(mp);
                mp.play();
                break;

            case 9:
                BtnAnswer4.setText(odgovor.questEurope.get(8));
                BtnAnswer2.setText(odgovor.questasia.get(ThreadLocalRandom.current().nextInt(1, 11 + 1)));
                BtnAnswer3.setText(odgovor.questAfrica.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer1.setText(odgovor.questAmericas.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer4.setOnAction(this::trueanswer);

                me = new Media(new File(hungary).toURI().toString());
                mp = new MediaPlayer(me);
                mv.setMediaPlayer(mp);
                mp.play();
                break;

            case 10:
                BtnAnswer3.setText(odgovor.questEurope.get(4));
                BtnAnswer2.setText(odgovor.questasia.get(ThreadLocalRandom.current().nextInt(1, 11 + 1)));
                BtnAnswer4.setText(odgovor.questAfrica.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer1.setText(odgovor.questAmericas.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer3.setOnAction(this::trueanswer);

                me = new Media(new File(britain).toURI().toString());
                mp = new MediaPlayer(me);
                mv.setMediaPlayer(mp);
                mp.play();
                break;

            case 11:
                BtnAnswer2.setText(odgovor.questEurope.get(2));
                BtnAnswer1.setText(odgovor.questasia.get(ThreadLocalRandom.current().nextInt(1, 11 + 1)));
                BtnAnswer3.setText(odgovor.questAfrica.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer4.setText(odgovor.questAmericas.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer2.setOnAction(this::trueanswer);

                me = new Media(new File(italy).toURI().toString());
                mp = new MediaPlayer(me);
                mv.setMediaPlayer(mp);
                mp.play();
                break;

            case 12:
                BtnAnswer2.setText(odgovor.questAmericas.get(1));
                BtnAnswer1.setText(odgovor.questasia.get(ThreadLocalRandom.current().nextInt(1, 11 + 1)));
                BtnAnswer3.setText(odgovor.questAfrica.get(ThreadLocalRandom.current().nextInt(1, 9 + 1)));
                BtnAnswer4.setText(odgovor.questEurope.get(ThreadLocalRandom.current().nextInt(1, 13 + 1)));
                BtnAnswer2.setOnAction(this::trueanswer);


                me = new Media(new File(usa).toURI().toString());
                mp = new MediaPlayer(me);
                mv.setMediaPlayer(mp);
                mp.play();
                break;


        }


    }

    @FXML
    private void setSceneChanger(ActionEvent event) throws IOException {
        Answer.setText("");
        mp.stop();
        BtnAnswer4.setDisable(false);
        BtnAnswer3.setDisable(false);
        BtnAnswer2.setDisable(false);
        BtnAnswer1.setDisable(false);


        Random questionchange =  new Random();
        int questgen = questionchange.nextInt(11+1);
        switchScene(questgen);


    }


    public void falseAnswer(ActionEvent event) throws IOException {
        Answer.setText("Wrong Answer");
        mp.stop();
        BtnAnswer4.setDisable(true);
        BtnAnswer3.setDisable(true);
        BtnAnswer2.setDisable(true);
        BtnAnswer1.setDisable(true);
        SceneChanger.setDisable(true);
    }

    private void trueanswer(ActionEvent event) {

        Answer.setText("Correct");
        mp.stop();
        BtnAnswer4.setDisable(true);
        BtnAnswer3.setDisable(true);
        BtnAnswer2.setDisable(true);
        BtnAnswer1.setDisable(true);
        SceneChanger.setDisable(false);


    }


}
























    










